program arr_test
      use iso_c_binding
      implicit none

      integer, parameter :: ITEMS = 3
      integer(C_INT) :: ITEMS_C = ITEMS
      integer, dimension(ITEMS) :: strlens = (/12, 5, 1/)

      type(C_PTR) :: arr_ptr

      ! Iterative Stuff
      type(C_PTR) :: ptr_arr(ITEMS)
      integer :: iter1, iter2
      integer(C_INT) :: longest
      integer(C_INT) :: iter_c
      type(C_PTR) :: ptr_temp
      character(len=:), pointer :: char_temp
      character(len=:), pointer :: char_final(:)
      type(C_PTR) :: cur_str

      print *, "C String Array Example"

      call populate_arr(arr_ptr)

      call make_fortran_arr(char_final, ITEMS, arr_ptr)
      
      !call copy_str(char_final, char_temp, 1)

      print *, "END OF PROGRAM!"

      !print *, char_final

      call print_arr(char_final, ITEMS)

      contains
                subroutine make_fortran_arr(arr, n_strings, c_arr_ptr)
                        use iso_c_binding
                        implicit none

                        character(len=:), pointer, intent(inout) :: arr(:)
                        integer, intent(in) :: n_strings
                        
                        type(C_PTR) :: c_arr_ptr
                        character(len=:), pointer :: char_temp

                        integer :: iter
                        
                        call get_longest_str(longest, items_c, arr_ptr)
                        print *, "longest string has length ", longest

                        call alloc_arr(arr, n_strings, longest)

                        do iter = 1, n_strings, 1
                                call get_str_at_index(cur_str, c_arr_ptr, iter)
                                allocate(character(len=strlens(iter)) :: char_temp)
                                call c_f_pointer(cur_str, char_temp)
                                print *, "Converted:", char_temp
                                call copy_str(arr, char_temp, iter)
                        end do


                end subroutine make_fortran_arr

                subroutine alloc_arr(arr, n_str, length)
                        implicit none

                        character(len=:), pointer, intent(inout) :: arr(:)
                        integer, intent(in) :: n_str    ! number of strings
                        integer, intent(in) :: length   ! Length of string array

                        allocate(character(len=length) :: arr(n_str))

                        
                end subroutine alloc_arr

                subroutine copy_str(arr, str_tmp, idx)
                        implicit none

                        character(len=:), pointer, intent(inout) :: arr(:)
                        character(len=:), pointer, intent(in) :: str_tmp
                        integer, intent(in) :: idx

                        integer :: iter
                        integer :: length
                        
                        length = len(str_tmp)
                        print *, "Copying string:", str_tmp

                        arr(idx)(1:length) = str_tmp
                        !do iter = 1, length, 1
                        !        arr(idx)(iter) = str_tmp(iter)
                        !end do

                end subroutine copy_str

                subroutine print_arr(arr, n_str)
                        implicit none

                        character(len=:), pointer, intent(in) :: arr(:)
                        integer, intent(in) :: n_str

                        integer :: iter

                        print *, "Printing big array..."

                        do iter = 1, n_str, 1
                                print *, arr(iter)
                        end do

                end subroutine print_arr

end program arr_test


