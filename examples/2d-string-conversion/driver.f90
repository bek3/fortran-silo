program strarray
    use iso_c_binding
    use silofprivate
    implicit none

    ! Allocate 2-D character array. This is new feature. Don't use

    character(len=:), pointer :: big_array(:) => null()
    character, pointer :: string_i(:) => null()
    type(C_PTR) :: array_ptr
    type(C_PTR) :: temp_ptr
    integer :: iter = 0

    print *, "F: Running!"

    call get_string(array_ptr)

    print *, "F: Back in Fortran!"

    allocate(character(3) :: big_array(7))

    do iter = 0, 2
        call get_string_at(array_ptr, temp_ptr, iter)
        call print_c_string(temp_ptr)
        call c_f_pointer(temp_ptr, string_i, shape=[strlen(temp_ptr)])
        print *, "F: String in fortran:", string_i
        string_i => null()
    end do

    print *, "Big Array: "

end program strarray