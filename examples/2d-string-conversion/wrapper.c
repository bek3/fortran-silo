#include<stdio.h>
#include<stdlib.h>
#include<string.h>

const char* STR1 = "Test1";
const char* STR2 = "MyTest2";
const char* STR3 = "t3st";

void get_string_(char** string_out){
	char* string1;
	char* string2;
	char* string3;

	printf("C: In C!\n");

	string1 = (char*) malloc(sizeof(char) * strlen(STR1));
	string2 = (char*) malloc(sizeof(char) * strlen(STR2));
	string3 = (char*) malloc(sizeof(char) * strlen(STR3));

	strcpy(string1, STR1);
	strcpy(string2, STR2);
	strcpy(string3, STR3);

	string_out[0] = string1;
	string_out[1] = string2;
	string_out[2] = string3;
}

void get_string_at_(char** big_string, char* target, int* index) {
	printf("C: Getting string at %i\n", *index);
	target = big_string[*index];
	printf("C: Returning %s\n", target);
}

void print_c_string_(char* string){
    printf("C: string in C: %s\n", string);
}
