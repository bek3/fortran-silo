! Private module containing C silo data. Used as intermediate step while converting to Fortran data.
module fortransilo_private

    use iso_c_binding   ! C Bindings
    implicit none

    !!!!!!!!!!!!!!!!!!!!!!!!!
    !   TYPE DEFINITIONS    !
    !!!!!!!!!!!!!!!!!!!!!!!!!

    ! Silo DB_Quadmesh Type
    type, bind(C) :: db_quadmesh_c
        integer(C_INT)      :: id
        integer(C_INT)      :: block_no
        integer(C_INT)      :: group_no
        type(C_PTR)         :: name                ! char*
        integer(C_INT)      :: cycle
        integer(C_INT)      :: coord_sys
        integer(C_INT)      :: major_order
        type(C_PTR)         :: stride               ! int*
        integer(C_INT)      :: coordtype
        integer(C_INT)      :: facetype
        integer(C_INT)      :: planar
        type(C_PTR)         :: x_coords_sp            ! float*
        type(C_PTR)         :: y_coords_sp
        type(C_PTR)         :: z_coords_sp
        type(C_PTR)         :: x_coords_dp            ! double*
        type(C_PTR)         :: y_coords_dp
        type(C_PTR)         :: z_coords_dp
        integer(C_INT)      :: datatype
        real(C_FLOAT)       :: time
        real(C_DOUBLE)      :: dtime
        type(C_PTR)         :: min_extents          ! double*
        type(C_PTR)         :: max_extents          ! double*
        type(C_PTR)         :: xname
        type(C_PTR)         :: yname
        type(C_PTR)         :: zname
!        type(C_PTR)         :: units
        integer(C_INT)      :: ndims
        integer(C_INT)      :: nspace
        integer(C_INT)      :: nnodes
        type(C_PTR)         :: dims                 ! int*
        integer(C_INT)      :: origin
        type(C_PTR)         :: min_index            ! int*
        type(C_PTR)         :: max_index            ! int*
        type(C_PTR)         :: base_index           ! int*
        type(C_PTR)         :: start_index          ! int*
        type(C_PTR)         :: size_index           ! int*
        integer(C_INT)      :: guihide
!        type(C_PTR)         :: mrgtree_name         ! char*
!        type(C_PTR)         :: ghost_name_labels
!        type(C_PTR)         :: ghost_zone_labels
!        type(C_PTR)         :: alt_nodenum_vars
!        type(C_PTR)         :: alt_zonenum_vars
        integer(C_INT)      :: coords_size
    end type db_quadmesh_c

    ! Silo DB_Quadvar Type
    type, bind(c) :: db_quadvar_c
        integer(C_INT)      :: id
        type(C_PTR)         :: name
        type(C_PTR)         :: units
        type(C_PTR)         :: label
        integer(C_INT)      :: cycle
        integer(C_INT)      :: meshid
        type(C_PTR)         :: vals
        integer(C_INT)      :: datatype
        integer             :: nels
        integer             :: nvals
        integer             :: ndims
        type(C_PTR)         :: dims                 ! int pointer
        integer(C_INT)      :: major_order
        type(C_PTR)         :: stride               ! int pointer
        type(C_PTR)         :: min_index            ! int pointer
        type(C_PTR)         :: max_index            ! int pointer
        integer(C_INT)      :: origin
        real(C_FLOAT)       :: time
        real(C_DOUBLE)      :: dtime
        type(C_PTR)         :: align                ! Double pointer
!        type(C_PTR)         :: mixvals
        integer(C_INT)      :: mixlen
        integer(C_INT)      :: use_specmf
        integer(C_INT)      :: ascii_labels
        type(C_PTR)         :: meshname
        integer(C_INT)      :: guihide
        integer(C_INT)      :: conserved
        integer(C_INT)      :: extensive
        integer(C_INT)      :: centering
        real(C_DOUBLE)      :: missing_value
    end type db_quadvar_c

!    ! Silo DB_Multimesh type
    type, bind(c) :: db_multimesh_c
        integer(C_INT)      :: id
        integer(C_INT)      :: nblocks
        integer(C_INT)      :: ngroups
        type(C_PTR)         :: meshids                  ! int*
        type(C_PTR)         :: meshnames               ! char**
        type(C_PTR)         :: meshtypes               ! int*
        type(C_PTR)         :: dirids                  ! int*
        integer(C_INT)      :: blockorigin
        integer(C_INT)      :: grouporigin
        integer(C_INT)      :: extentssize
        type(C_PTR)         :: extents                 ! double*
        type(C_PTR)         :: zonecounts              ! int*
        type(C_PTR)         :: has_external_zones      ! int*
        integer(C_INT)      :: guihide
        integer(C_INT)      :: lgroupings
        type(C_PTR)         :: groupings               ! int*
!        type(C_PTR)         :: group_names             ! char**
        integer(C_INT)      :: tv_connectivity
        integer(C_INT)      :: disjoint_mode
        integer(C_INT)      :: topo_dim
        type(C_PTR)         :: file_ns                 ! char*
        type(C_PTR)         :: block_ns                ! char*
        integer(C_INT)      :: block_type
        type(C_PTR)         :: empty_list              ! int*
        integer(C_INT)      :: empty_cnt
        integer(C_INT)      :: repr_block_idx
!        type(C_PTR)         :: alt_nodenum_vars        ! char**
!        type(C_PTR)         :: alt_zonenum_vars        ! char**
!        type(C_PTR)         :: meshnames_alloc         ! char*
    end type db_multimesh_c

    ! Silo DB_Multivar type
    type, bind(c) :: db_multivar_c
        integer(C_INT)      :: id
        integer(C_INT)      :: nvars
        integer(C_INT)      :: ngroups
        type(C_PTR)         :: varnames               ! char** pointer
        type(C_PTR)         :: vartypes               ! int pointer
        integer(C_INT)      :: blockorigin
        integer(C_INT)      :: grouporigin
        integer(C_INT)      :: extentssize
        type(C_PTR)         :: extents                ! double pointer
        integer(C_INT)      :: guihide
!        type(C_PTR)         :: region_names           ! char** pointer
        type(C_PTR)         :: mmesh_name             ! char pointer
        integer(C_INT)      :: tensor_rank
        integer(C_INT)      :: conserved
        integer(C_INT)      :: extensive
        type(C_PTR)         :: file_ns                ! char pointer
        type(C_PTR)         :: block_ns               ! char pointer
        integer(C_INT)      :: block_type
        type(C_PTR)         :: empty_list             ! int pointer
        integer(C_INT)      :: empty_cnt
        integer(C_INT)      :: repr_block_idx
        real(C_DOUBLE)      :: missing_value
        type(C_PTR)         :: varnames_alloc
    end type db_multivar_c

    !!!!!!!!!!!!!!!!!!!!!
    !   C INTERFACES    !
    !!!!!!!!!!!!!!!!!!!!!

    ! Call DBGetVar
    interface
       subroutine load_db_getvar_c(dbfile, var_name, name_len, var) bind(c)
         use iso_c_binding
         integer(C_INT), intent(in) :: dbfile
         character, intent(in) :: var_name
         integer, intent(in) :: name_len
         real(C_DOUBLE), intent(out) :: var
       end subroutine load_db_getvar_c
    end interface
         
    ! Call DBGetQuadvar
    interface
        subroutine load_db_quadmesh_c(dbfile, qmesh_name, name_len, qmesh_out, optlist_id) bind(c)
            use iso_c_binding
            import :: db_quadmesh_c
            integer(C_INT), intent(in) :: dbfile                    ! Identifier for open Silo file
            character, intent(in) :: qmesh_name
            integer, intent(in) :: name_len
            type(db_quadmesh_c), intent(out) :: qmesh_out           ! Recieved quadmesh
            integer(C_INT), intent(out) :: optlist_id
        end subroutine
    end interface

    ! Call DBGetQuadvar
    interface
        subroutine load_db_quadvar_c(dbfile, qvar_name, name_len, qvar_out, qv_optlist,qvar_err) bind(c)
            use iso_c_binding
            import :: db_quadvar_c
            integer(C_INT), intent(in) :: dbfile                    ! Identifier for open Silo file
            character, intent(in) :: qvar_name
            integer, intent(in) :: name_len
            type(db_quadvar_c), intent(out) :: qvar_out             ! Recieved quadvariable
            integer(C_INT), intent(in) :: qv_optlist
            integer(C_INT), intent(out) :: qvar_err
        end subroutine
    end interface

    ! Call DBGetMultimesh
    interface
        subroutine load_db_multimesh_c(dbfile, mmesh_name, name_len, mmesh_out, mm_optlist) bind(c)
            use iso_c_binding
            import :: db_multimesh_c
            integer(C_INT), intent(in) :: dbfile                    ! Identifier for open Silo file
            character, intent(in) :: mmesh_name
            integer, intent(in) :: name_len
            type(db_multimesh_c), intent(out) :: mmesh_out          ! Recieved multimesh
            integer(C_INT), intent(in) :: mm_optlist
        end subroutine
    end interface

    ! Call DBGetMultivar
    interface
        subroutine load_db_multivar_c(dbfile, mvar_name, name_len, mvar_out, mm_optlist) bind(c)
            use iso_c_binding
            import :: db_multivar_c
            integer(C_INT), intent(in) :: dbfile                    ! Identifier for open Silo file
            character, intent(in) :: mvar_name
            integer, intent(in) :: name_len
            type(db_multivar_c), intent(out) :: mvar_out            ! Recieved multivariable
            integer(C_INT), intent(in) :: mm_optlist
        end subroutine
    end interface

    interface
        subroutine get_opt_i_c(optlist_id, opt, val) bind(c)
            use iso_c_binding
            integer(C_INT), intent(in) :: optlist_id
            integer(C_INT), intent(in) :: opt
            integer(C_INT), intent(out) :: val
        end subroutine get_opt_i_c
    end interface

    interface
        subroutine get_opt_i_arr_c(optlist_id, opt, val) bind(c)
            use iso_c_binding
            integer(C_INT), intent(in) :: optlist_id
            integer(C_INT), intent(in) :: opt
            type(C_PTR), intent(out) :: val
        end subroutine get_opt_i_arr_c
    end interface

    interface
        subroutine get_opt_r_c(optlist_id, opt, val) bind(c)
            use iso_c_binding
            integer(C_INT), intent(in) :: optlist_id
            integer(C_INT), intent(in) :: opt
            real(C_FLOAT), intent(out) :: val
        end subroutine get_opt_r_c
    end interface

    interface
        subroutine get_opt_d_c(optlist_id, opt, val) bind(c)
            use iso_c_binding
            integer(C_INT), intent(in) :: optlist_id
            integer(C_INT), intent(in) :: opt
            real(C_DOUBLE), intent(out) :: val
        end subroutine get_opt_d_c
    end interface

    interface
        subroutine get_opt_d_arr_c(optlist_id, opt, val) bind(c)
            use iso_c_binding
            integer(C_INT), intent(in) :: optlist_id
            integer(C_INT), intent(in) :: opt
            type(C_PTR), intent(out) :: val
        end subroutine get_opt_d_arr_c
    end interface

    interface
        subroutine get_opt_str_c(optlist_id, opt, val) bind(c)
            use iso_c_binding
            integer(C_INT), intent(in) :: optlist_id
            integer(C_INT), intent(in) :: opt
            type(C_PTR), intent(out) :: val
        end subroutine get_opt_str_c
    end interface


    ! Call strlen
    interface
        integer(c_size_t) function strlen(str) bind(c)
            use, intrinsic :: iso_c_binding, only: c_ptr, c_size_t
            type(c_ptr), intent(in), value :: str                   ! String to be counted
        end function
    end interface

end module fortransilo_private
