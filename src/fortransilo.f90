! Main module for Fortransilo. Contains silo datatypes in native fortran types and get methods.
module fortransilo

    use fortransilo_private     ! Private module
    use dbstring                ! Module for handling strings
    use optlist                 ! Module for handling optlists
    use iso_c_binding           ! C Bindings
    implicit none

    ! Silo DB_Quadmesh Type
    type db_quadmesh_final
        integer                     :: id
        type(db_string)             :: name
        integer                     :: coordtype
        real(kind=8), pointer       :: coords_dp(:) => null()
        integer                     :: datatyp
        integer                     :: ndims
        integer, pointer            :: dims(:) => null()
    end type db_quadmesh_final

    ! Silo DB_Quadvar Type
    type db_quadvar_final
        integer                     :: id
        type(db_string)             :: name
        real, pointer               :: vals(:) => null()
        integer                     :: datatype
        integer                     :: nvals
        integer                     :: ndims
        integer, pointer            :: dims(:) => null()
        integer                     :: mixlen
        integer                     :: centering
    end type db_quadvar_final

    interface check_error
        module procedure check_error_m
        module procedure check_error_nm
    end interface check_error

    ! ###################################
    ! FUNCTIONS TO CALL FROM OUTSIDE
    ! ###################################

    contains

        ! Retrive variable with DP
        integer function dbgetvar(dbid, name, var) result(err)
          use iso_c_binding
          use fortransilo_private
          use optlist
          implicit none
          include "silo_f9x.inc"
          integer, intent(in) :: dbid
          character(len=*), intent(in) :: name
          real(kind=8), intent(out) :: var

          call load_db_getvar_c(dbid, name, len(trim(name)),var)

          err = 0

        end function dbgetvar
      
        ! Retrieve Quadmesh with DP Coords
        integer function dbgetqm(dbid, name, xname, yname, zname, x_dp, y_dp, z_dp, &
                & dims, ndims, datatype, coordtype, qm_optlist) result(err)
            use iso_c_binding
            use fortransilo_private
            use optlist
            implicit none
            include "silo_f9x.inc"

            integer, intent(in) :: dbid
            character(len=*), intent(in) :: name
            character(len=:), pointer, intent(out) :: xname
            character(len=:), pointer, intent(out) :: yname
            character(len=:), pointer, intent(out) :: zname
            real(kind=8), pointer, intent(out) :: x_dp(:)
            real(kind=8), pointer, intent(out) :: y_dp(:)
            real(kind=8), pointer, intent(out) :: z_dp(:)
            integer, pointer, intent(out) :: dims(:)
            integer, intent(out) :: ndims
            integer, intent(out) :: datatype
            integer, intent(out) :: coordtype
            integer, optional, intent(inout) :: qm_optlist

            integer :: iter

            type(db_string) :: db_xname
            type(db_string) :: db_yname
            type(db_string) :: db_zname

            type(db_quadmesh_final) :: qmesh_out
            type(db_quadmesh_c) :: quadmesh_in

            ! Temporary arrays
            integer, pointer :: base_index(:)

            integer :: opt = DB_F77NULL

            if (present(qm_optlist)) then
                call load_db_quadmesh_c(dbid, name, len(trim(name)), quadmesh_in, qm_optlist)
            else
                call load_db_quadmesh_c(dbid, name, len(trim(name)), quadmesh_in, opt)
            end if

            ! Primitives
            ndims = int(quadmesh_in%ndims)
            datatype = int(quadmesh_in%datatype)
            coordtype = int(quadmesh_in%coordtype)

            ! Dims array
            ! Note: This will be 1 indexed
            call c_f_pointer(quadmesh_in%dims, dims, shape=[ndims])

            ! Coordinates conversions
            call c_f_pointer(quadmesh_in%x_coords_dp, x_dp, shape=[dims(1)])
            if(ndims > 1) then
                call c_f_pointer(quadmesh_in%y_coords_dp, y_dp, shape=[dims(2)])
                if(ndims > 2) then
                    call c_f_pointer(quadmesh_in%z_coords_dp, z_dp, shape=[dims(3)])
                else
                    allocate(z_dp(0))
                end if
            else
                allocate(y_dp(0))
                allocate(z_dp(0))
            end if

            ! Get Mesh Name
            call parse_c_string(qmesh_out%name, quadmesh_in%name)
            !            name => qmesh_out%name%string

            ! Get X axis name
            allocate(character(len=strlen(quadmesh_in%xname)) :: xname)
            call c_f_pointer(quadmesh_in%xname, xname)

            ! Get Y axis name
            allocate(character(len=strlen(quadmesh_in%yname)) :: yname)
            call c_f_pointer(quadmesh_in%yname, yname)

            ! Get Z axis name
            allocate(character(len=strlen(quadmesh_in%zname)) :: zname)
            call c_f_pointer(quadmesh_in%zname, zname)

            err = 0

        end function dbgetqm

        ! Retrieve 3D Single Precision Quadvar
        integer function dbgetqv(dbid, name, mesh_name, nvars, varnames, vars, &
                 dims, ndims, mixvar, mixlen, datatype, centering, qv_optlist) result(err)
            use iso_c_binding
            use fortransilo_private
            use dbstring
            implicit none
            include "silo_f9x.inc"

            integer, intent(in) :: dbid
            character(len=*), intent(in) :: name
            character(len=:), pointer, intent(out) :: mesh_name
            integer, intent(out) :: nvars
            character(len=:), pointer, intent(out) :: varnames(:)
            real(kind=8), pointer, intent(out) :: vars(:,:,:)
            integer, pointer, intent(out) :: dims(:)
            integer, intent(out) :: ndims
            character, pointer, intent(out) :: mixvar
            integer, pointer, intent(out) :: mixlen(:)
            integer, intent(out) :: datatype
            integer, intent(out) :: centering
            integer, optional, intent(inout) :: qv_optlist

            type(db_quadvar_c) :: quadvar_in
            type(db_string) :: meshname_type

            integer :: opt = DB_F77NULL

            err = 0

            if (present(qv_optlist)) then
               call load_db_quadvar_c(dbid, name, len(trim(name)), quadvar_in, qv_optlist,err)
            else
               call load_db_quadvar_c(dbid, name, len(trim(name)), quadvar_in, opt      , err)
            end if

            ! Check for error
            if (err.ne.0) return

            ! Mesh Name
            allocate(character(len=strlen(quadvar_in%meshname)) :: mesh_name)
            call c_f_pointer(quadvar_in%meshname, mesh_name)

            nvars = quadvar_in%nvals

            ! TODO: Varnames

            ! Ndims
            ndims = int(quadvar_in%ndims) ! Should always be 3

            ! Dims
            call c_f_pointer(quadvar_in%dims, dims, shape=[ndims])

            ! Vars
            call c_f_pointer(quadvar_in%vals, vars, shape=[dims])

            ! TODO: Mixvar

            ! TODO: Mixlen

            ! Datatype
            datatype = int(quadvar_in%datatype)

            ! Centering
            centering = int(quadvar_in%centering)

        end function dbgetqv

        ! Retrieve Multimesh
        integer function dbgetmmesh(dbid, name, nmesh, meshnames, meshtypes, optlist) result(err)
            use iso_c_binding
            use fortransilo_stringhandler
            use fortransilo_private
            implicit none
            include "silo_f9x.inc"

            integer, intent(in) :: dbid
            character(len=*), intent(in) :: name
            integer, intent(out) :: nmesh
            character(len=:), pointer, intent(out) :: meshnames(:)
            integer, pointer, intent(out) :: meshtypes(:)
            integer, optional, intent(inout) :: optlist

            integer :: opt = DB_F77NULL

            type(db_multimesh_c) :: multimesh_in

            if (present(optlist)) then
                call load_db_multimesh_c(dbid, name, len(trim(name)), multimesh_in, optlist)
            else
                call load_db_multimesh_c(dbid, name, len(trim(name)), multimesh_in, opt)
            end if

            nmesh = int(multimesh_in%nblocks)

            call c_f_pointer(multimesh_in%meshtypes, meshtypes, shape=[nmesh])

            err = populate_arr_2d(meshnames, nmesh, multimesh_in%meshnames)
            call check_error(err)

        end function dbgetmmesh

        integer function dbgetmvar(dbid, name, nvar, varnames, vartypes, optlist) result(err)
            use iso_c_binding
            use fortransilo_stringhandler
            use fortransilo_private
            implicit none
            include "silo_f9x.inc"

            integer, intent(in) :: dbid
            character(len=*), intent(in) :: name
            integer, intent(out) :: nvar
            character(len=:), pointer, intent(out) :: varnames(:)
            integer, pointer, intent(out) :: vartypes(:)
            integer, optional, intent(inout) :: optlist

            type(db_multivar_c) :: mvar_in

            integer :: opt = DB_F77NULL

            if (present(optlist)) then
                call load_db_multivar_c(dbid, name, len(trim(name)), mvar_in, optlist)
            else
                call load_db_multivar_c(dbid, name, len(trim(name)), mvar_in, opt)
            end if

            nvar = int(mvar_in%nvars)

            call c_f_pointer(mvar_in%vartypes, vartypes, shape=[nvar])

            err = populate_arr_2d(varnames, nvar, mvar_in%varnames)

        end function dbgetmvar


        ! Check for error code with message
        subroutine check_error_m(err, message)
            implicit none

            integer, intent(inout) :: err
            character(len=*), intent(in) :: message

            if(err /= 0) then
                print *, "Error detected! Code ", err, message
            end if

            err = 0

        end subroutine check_error_m

        ! Check for error code
        subroutine check_error_nm(err)
            implicit none

            integer, intent(inout) :: err

            if(err /= 0) then
                print *, "Error detected! Code ", err
            end if

            err = 0

        end subroutine check_error_nm

        ! Library Test Subroutine. To be deleted soon
        integer function lib_test_() result(return)

                return = 15
                print *, "Running Libtest!"

        end function

end module fortransilo
