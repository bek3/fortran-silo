cmake_minimum_required(VERSION 3.8)
project(fs-test)
enable_language(Fortran)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/../cmake/")

find_package(Silo)
find_package(HDF5)
find_package(SZIP)

include_directories(/usr/local/include/)

# TODO: Don't hardcode paths
add_executable(demo.out fortransilo-demo.f90)
target_link_libraries(demo.out /usr/local/include/static/libfortransilo.a ${HDF5_LIBRARY} ${SZIP_LIBRARY} ${Silo_LIBRARY} stdc++ z)
