! Program to test the library's ability to read several silo files into Fortran.

! Checkout commit hash 825cb89 for int array examples

program testsilo
    use fortransilo         ! My Module
    use unit_test           ! Unit Test Framework
    use constants           ! fortran-silo Constants
    implicit none
    include "silo_f9x.inc"  ! LLNL Silo

    integer :: ierr, err, iter

    !!!!!!!!!!!!!!!!! CONFIGURE TEST !!!!!!!!!!!!!!!
    call init_test()

    !!!!!!!!!!!!!!!!! CREATE FILES !!!!!!!!!!!!!!!

    print *, "+-------------------------------------------------------------+"
    print *, "| Creating Test Files                                         |"
    print *, "+-------------------------------------------------------------+"

    print *, "Creating test file 1"
    err = dbcreate(filename1,14,DB_CLOBBER,DB_LOCAL,d3_qmesh_desc,23,DB_HDF5,file1)
    call check_error(err, "create file 1")

    err = dbclose(file1)
    call check_error(err, "close file 1")

    !!!!!!!!!!!!!!!!! PUT DATA !!!!!!!!!!!!!!

    print *, "+-------------------------------------------------------------+"
    print *, "| Writing Test Data                                           |"
    print *, "+-------------------------------------------------------------+"

    print *, "Opening Output File 1"
    err = dbopen(filename1, len(filename1), DB_HDF5, DB_APPEND, file1)
    call check_error(err, "open file 1")

    ! Write Double Precision 3D Quadmesh
    print *, "Writing Double Precision 3D quadmesh"
    err = dbmkoptlist(SILO_OPTLIST_MAX_SIZE, out_d3_qmesh_optlist)
    call check_error(err)
    err = dbaddiopt(out_d3_qmesh_optlist, DBOPT_GROUPNUM, out_d3_qmesh_groupno)
    call check_error(err)
    err = dbaddiopt(out_d3_qmesh_optlist, DBOPT_CYCLE, out_d3_qmesh_cycle)
    call check_error(err)
    err = dbaddiopt(out_d3_qmesh_optlist, DBOPT_COORDSYS, out_d3_qmesh_majororder)
    call check_error(err)
    err = dbaddiopt(out_d3_qmesh_optlist, DBOPT_MAJORORDER, out_d3_qmesh_majororder)
    call check_error(err)
    err = dbaddiopt(out_d3_qmesh_optlist, DBOPT_FACETYPE, out_d3_qmesh_facetype)
    call check_error(err)
    err = dbaddiopt(out_d3_qmesh_optlist, DBOPT_PLANAR, out_d3_qmesh_planar)
    call check_error(err)
    err = dbaddropt(out_d3_qmesh_optlist, DBOPT_TIME,out_d3_qmesh_time)
    call check_error(err)
    err = dbadddopt(out_d3_qmesh_optlist, DBOPT_DTIME, out_d3_qmesh_dtime)
    call check_error(err)
    err = dbaddiopt(out_d3_qmesh_optlist, DBOPT_NSPACE, out_d3_qmesh_nspace)
    call check_error(err)
    err = dbaddiopt(out_d3_qmesh_optlist, DBOPT_ORIGIN, out_d3_qmesh_origin)
    call check_error(err)
    err = dbputqm(file1, d3_qmesh_name, len(d3_qmesh_name), out_d3_qmesh_xname, 6, &
            & out_d3_qmesh_yname, 6, out_d3_qmesh_zname, 6, out_d3_qmesh_x, out_d3_qmesh_y, out_d3_qmesh_z, &
            & out_d3_qmesh_dims, out_d3_qmesh_ndims, DB_DOUBLE, DB_COLLINEAR, out_d3_qmesh_optlist, ierr)
    call check_error(err)

    print *, "Writing Double Precision 3D quadvar"
    err = dbmkoptlist(SILO_OPTLIST_MAX_SIZE, out_d3_qvar_optlist)
    call check_error(err)

!    err = dbmkoptlist(SILO_OPTLIST_MAX_SIZE, out_d3_qvar_optlist)
    err = dbaddiopt(out_d3_qvar_optlist, DBOPT_COORDSYS, out_d3_qvar_coordsys)
    call check_error(err)
    err = dbaddiopt(out_d3_qvar_optlist, DBOPT_CYCLE, out_d3_qvar_cycle)
    call check_error(err)
    err = dbaddiopt(out_d3_qvar_optlist, DBOPT_FACETYPE, out_d3_qvar_facetype)
    call check_error(err)
    err = dbaddiopt(out_d3_qvar_optlist, DBOPT_MAJORORDER, out_d3_qvar_majororder)
    call check_error(err)
    err = dbaddiopt(out_d3_qvar_optlist, DBOPT_ORIGIN, out_d3_qvar_origin)
    call check_error(err)
    err = dbaddropt(out_d3_qvar_optlist, DBOPT_TIME, out_d3_qvar_time)
    call check_error(err)
    err = dbadddopt(out_d3_qvar_optlist, DBOPT_DTIME, out_d3_qvar_dtime)
    call check_error(err)
    err = dbaddiopt(out_d3_qvar_optlist, DBOPT_USESPECMF, out_d3_qvar_usespecmf)
    call check_error(err)
    err = dbaddiopt(out_d3_qvar_optlist, DBOPT_ASCII_LABEL, out_d3_qvar_asciilabel)
    call check_error(err)
    err = dbaddiopt(out_d3_qvar_optlist, DBOPT_CONSERVED, out_d3_qvar_conserved)
    call check_error(err)
    err = dbaddiopt(out_d3_qvar_optlist, DBOPT_EXTENSIVE, out_d3_qvar_extensive)
    call check_error(err)
    err = dbaddiopt(out_d3_qvar_optlist, DBOPT_HIDE_FROM_GUI, out_d3_qvar_guihide)
    call check_error(err)

    err = populate_arr(out_d3_qvar_vars, out_d3_qvar_dims)
    call check_error(err)
!    print *, "varnames:", out_d3_qvar_varnames
!    print *, "Vars:", out_d3_qvar_vars

    err = dbputqv(file1, out_d3_qvar_name, len(out_d3_qvar_name), out_d3_qvar_meshname, len(out_d3_qvar_meshname), &
            & out_d3_qvar_nvars, out_d3_qvar_varnames, out_d3_qvar_lvarnames, out_d3_qvar_vars, out_d3_Qvar_dims, &
            & out_d3_qvar_ndims, DB_F77NULL, out_d3_qvar_mixlen, DB_DOUBLE, DB_ZONECENT, out_d3_qvar_optlist, &
            & ierr)
    call check_error(err)

    ! MULTIMESH
    err = dbmkoptlist(SILO_OPTLIST_MAX_SIZE, out_mmesh_optlist)
    call check_error(err)

    err = dbaddiopt(out_mmesh_optlist, DBOPT_BLOCKORIGIN, out_mmesh_blockorigin)
    call check_error(err)
    err = dbaddiopt(out_mmesh_optlist, DBOPT_CYCLE, out_mmesh_cycle)
    call check_error(err)
    err = dbaddropt(out_mmesh_optlist, DBOPT_TIME, out_mmesh_time)
    call check_error(err)
    err = dbadddopt(out_mmesh_optlist, DBOPT_DTIME, out_mmesh_dtime)
    call check_error(err)
    err = dbaddiopt(out_mmesh_optlist, DBOPT_EXTENTS_SIZE, out_mmesh_extentssize)
    call check_error(err)
    err = dbaddiopt(out_mmesh_optlist, DBOPT_HIDE_FROM_GUI, out_mmesh_guihide)
    call check_error(err)
    err = dbaddiopt(out_mmesh_optlist, DBOPT_TV_CONNECTIVITY, out_mmesh_tvconnectivity)
    call check_error(err)
    err = dbaddiopt(out_mmesh_optlist, DBOPT_DISJOINT_MODE, out_mmesh_disjoint)
    call check_error(err)
    err = dbaddiopt(out_mmesh_optlist, DBOPT_TOPO_DIM, out_mmesh_topodim)
    call check_error(err)
    err = dbaddiopt(out_mmesh_optlist, DBOPT_MB_BLOCK_TYPE, out_mmesh_mbblocktype)
    call check_error(err)
    err = dbaddiopt(out_mmesh_optlist, DBOPT_MB_EMPTY_COUNT, out_mmesh_mbemptycount)
    call check_error(err)

    allocate(out_mmesh_meshnames(out_mmesh_mesh))
    print *, "Number of meshes: ", out_mmesh_mesh
!    out_mmesh_meshnames(1) = "test1"
!    out_mmesh_meshnames(2) = "test2"
!    out_mmesh_meshnames(3) = "test3"
    write(out_mmesh_meshnames(1), '(a)') "test333"
    write(out_mmesh_meshnames(2), '(a)') "test22"
    write(out_mmesh_meshnames(3), '(a)') "test1"
    print*,'done with defining mmmesh_meshnames'
    err = dbset2dstrlen(len(out_mmesh_meshnames))
    call check_error(err)

    print *, "writing... ", out_mmesh_meshnames

    err = dbputmmesh(file1, out_mmesh_name, len(out_mmesh_name), out_mmesh_mesh, out_mmesh_meshnames, &
            & out_mmesh_meshname_len, out_mmesh_meshtypes, out_mmesh_optlist, ierr)
    call check_error(err)

    ! MULTIVAR
    err = dbmkoptlist(SILO_OPTLIST_MAX_SIZE, out_mvar_optlist)
    call check_error(err)

    err = dbaddiopt(out_mvar_optlist, DBOPT_BLOCKORIGIN, out_mvar_blockorigin)
    call check_error(err)
    err = dbaddiopt(out_mvar_optlist, DBOPT_CYCLE, out_mvar_cycle)
    call check_error(err)
    err = dbaddropt(out_mvar_optlist, DBOPT_TIME, out_mvar_time)
    call check_error(err)
    err = dbadddopt(out_mvar_optlist, DBOPT_DTIME, out_mvar_dtime)
    call check_error(err)
    err = dbaddiopt(out_mvar_optlist, DBOPT_HIDE_FROM_GUI, out_mvar_guihide)
    call check_error(err)
    err = dbaddiopt(out_mvar_optlist, DBOPT_EXTENTS_SIZE, out_mvar_extentssize)
    call check_error(err)
    err = dbaddiopt(out_mvar_optlist, DBOPT_TENSOR_RANK, out_mvar_tensorrank)
    call check_error(err)
    err = dbaddiopt(out_mvar_optlist, DBOPT_CONSERVED, out_mvar_conserved)
    call check_error(err)
    err = dbaddiopt(out_mvar_optlist, DBOPT_EXTENSIVE, out_mvar_extensive)
    call check_error(err)
    err = dbaddiopt(out_mvar_optlist, DBOPT_MB_BLOCK_TYPE, out_mvar_mbblocktype)
    call check_error(err)
    err = dbaddiopt(out_mvar_optlist, DBOPT_MB_EMPTY_COUNT, out_mvar_emptycount)
    call check_error(err)
    err = dbaddiopt(out_mvar_optlist, DBOPT_MISSING_VALUE, out_mvar_missing)
    call check_error(err)

    allocate(out_mvar_varnames(out_mvar_nvar))
    print *, "Number of vars: ", out_mvar_nvar
    write(out_mvar_varnames(1), '(a)') "333test"
    write(out_mvar_varnames(2), '(a)') "22test"
    write(out_mvar_varnames(3), '(a)') "1test"
    print *, "Done defining varnames"

    print *, "Writing... ", out_mvar_varnames

    err = dbputmvar(file1, out_mvar_name, len(out_mvar_name), out_mvar_nvar, out_mvar_varnames, &
        & out_mvar_varname_len, out_mvar_vartypes, out_mvar_optlist, ierr)
    call check_error(err)

    ! WRAP UP
    err = dbclose(file1)
    call check_error(err)
    
    print *, "Reopening File 1"
    err = dbopen(filename1, len(filename1), DB_HDF5, DB_APPEND, file1)
    call check_error(err, "open file 1")

   
    print *, "+-------------------------------------------------------------+"
    print *, "| Reading files                                               |"
    print *, "+-------------------------------------------------------------+"

    ! Read Double Precision 3D quadmesh
    print *, "Retrieving 3D Double Precision Quadmesh ", d3_qmesh_name
    err = dbgetqm(file1,d3_qmesh_name, in_d3_qmesh_xname, in_d3_qmesh_yname, in_d3_qmesh_zname, &
            &  in_d3_qmesh_x, in_d3_qmesh_y, in_d3_qmesh_z, in_d3_qmesh_dims, &
            & in_d3_qmesh_ndims, in_d3_qmesh_datatype, in_d3_qmesh_coordtype, in_d3_qmesh_optlist)
    call check_error(err)
    err = dbgetopt(in_d3_qmesh_optlist, DBOPT_GROUPNUM, in_d3_qmesh_groupno)
    call check_error(err)
    err = dbgetopt(in_d3_qmesh_optlist, DBOPT_CYCLE, in_d3_qmesh_cycle)
    call check_error(err)
    err = dbgetopt(in_d3_qmesh_optlist, DBOPT_COORDSYS, in_d3_qmesh_coordsys)
    call check_error(err)
    err = dbgetopt(in_d3_qmesh_optlist, DBOPT_MAJORORDER, in_d3_qmesh_majororder)
    call check_error(err)
    err = dbgetopt(in_d3_qmesh_optlist, DBOPT_FACETYPE, in_d3_qmesh_facetype)
    call check_error(err)
    err = dbgetopt(in_d3_qmesh_optlist, DBOPT_PLANAR, in_d3_qmesh_planar)
    call check_error(err)
    err = dbgetopt(in_d3_qmesh_optlist, DBOPT_TIME, in_d3_qmesh_time)
    call check_error(err)
    err = dbgetopt(in_d3_qmesh_optlist, DBOPT_DTIME, in_d3_qmesh_dtime)
    call check_error(err)
    err = dbgetopt(in_d3_qmesh_optlist, DBOPT_NSPACE, in_d3_qmesh_nspace)
    call check_error(err)
    err = dbgetopt(in_d3_qmesh_optlist, DBOPT_ORIGIN, in_d3_qmesh_origin)
    call check_error(err)
    err = dbgetopt(in_d3_qmesh_optlist, DBOPT_BASEINDEX, in_d3_qmesh_baseindex)
    call check_error(err)

    ! Test 3D DP Quadmesh
    call compare(130, in_d3_qmesh_coordtype, "3D DP Quadmesh Coordtype")
    call compare(DB_DOUBLE, in_d3_qmesh_datatype, "3D DP Quadmesh Datatype")
    call compare(out_d3_qmesh_ndims, in_d3_qmesh_ndims, "3D DP Quadmesh NDims")
    call compare(out_d3_qmesh_dims, in_d3_qmesh_dims, "3D DP Quadmesh Dims")
    call compare(out_d3_qmesh_x, in_d3_qmesh_x, "3D DP Quadmesh X Coords")
    call compare(out_d3_qmesh_y, in_d3_qmesh_y, "3D DP Quadmesh Y Coords")
    call compare(out_d3_qmesh_z, in_d3_qmesh_z, "3D DP Quadmesh Z Coords")
    call compare(out_d3_qmesh_xname, in_d3_qmesh_xname, "3D DP Quadmesh X Label")
    call compare(out_d3_qmesh_yname, in_d3_qmesh_yname, "3D DP Quadmesh Y Label")
    call compare(out_d3_qmesh_zname, in_d3_qmesh_zname, "3D DP Quadmesh Z Label")
    call compare(out_d3_qmesh_groupno, in_d3_qmesh_groupno, "3D DP Quadmesh Group Number - OPTION")
    call compare(out_d3_qmesh_cycle, in_d3_qmesh_cycle, "3D DP Quadmesh Cycle - OPTION ")
    call compare(out_d3_qmesh_coordsys, in_d3_qmesh_coordsys, "3D DP Quadmesh Coordsys - OPTION")
    call compare(out_d3_qmesh_majororder, in_d3_qmesh_majororder, "3D DP Quadmesh Major Order - OPTION")
    call compare(out_d3_qmesh_facetype, in_d3_qmesh_facetype, "3D DP Quadmesh facetype - OPTION")
    call compare(out_d3_qmesh_planar, in_d3_qmesh_planar, "3D DP Quadmesh Planar - OPTION")
    call compare(out_d3_qmesh_time, in_d3_qmesh_time, "3D DP Quadmesh Time - OPTION")
    call compare(out_d3_qmesh_dtime, in_d3_qmesh_dtime, "3D DP Quadmesh DTime - OPTION")
    call compare(out_d3_qmesh_nspace, in_d3_qmesh_nspace, "3D DP Quadmesh NDPace - OPTION")
    call compare(out_d3_qmesh_origin, in_d3_qmesh_origin, "3D DP Quamesh Origin - OPTION")
    call compare(out_d3_qmesh_baseindex, in_d3_qmesh_baseindex, "3D DP Quadmesh Base Index")

    !!!!!!!!!!!!!!!!! TEST FILES !!!!!!!!!!!!!!!

    print *, "Retrieving 3D Double Precision var ", out_d3_qvar_name
    err = dbgetqv(file1, out_d3_qvar_name, in_d3_qvar_meshname, in_d3_qvar_nvars, in_d3_qvar_varnames, &
            & in_d3_qvar_vars, in_d3_qvar_dims, in_d3_qvar_ndims, in_d3_qvar_mixvars, in_d3_qvar_mixlen, &
            & in_d3_qvar_datatype, in_d3_qvar_centering, in_d3_qvar_optlist)

    print *, "Retrieving 3D Double Precision Quadvar optlist"
    err = dbgetopt(in_d3_qvar_optlist, DBOPT_CYCLE, in_d3_qvar_cycle)
    call check_error(err)
    err = dbgetopt(in_d3_qvar_optlist, DBOPT_MAJORORDER, in_d3_qvar_majororder)
    call check_error(err)
    err = dbgetopt(in_d3_qvar_optlist, DBOPT_ORIGIN, in_d3_qvar_origin)
    call check_error(err)
    err = dbgetopt(in_d3_qvar_optlist, DBOPT_TIME, in_d3_qvar_time)
    call check_error(err)
    err = dbgetopt(in_d3_qvar_optlist, DBOPT_DTIME, in_d3_qvar_dtime)
    call check_error(err)
    err = dbgetopt(in_d3_qvar_optlist, DBOPT_ASCII_LABEL, in_d3_qvar_asciilabel)
    call check_error(err)
    err = dbgetopt(in_d3_qvar_optlist, DBOPT_CONSERVED, in_d3_qvar_conserved)
    call check_error(err)
    err = dbgetopt(in_d3_qvar_optlist, DBOPT_EXTENSIVE, in_d3_qvar_extensive)
    call check_error(err)
    err = dbgetopt(in_d3_qvar_optlist, DBOPT_HIDE_FROM_GUI, in_d3_qvar_guihide)
    call check_error(err)

    print *, "QV optlist: ", in_d3_qvar_optlist


    ! Test
    call compare(out_d3_qvar_nvars, in_d3_qvar_nvars, "Quadvar Nvars")
!    call compare(out_d3_qvar_varnames, in_d3_qvar_varnames, "Quadvar Varnames")
    call compare(out_d3_qvar_dims, in_d3_qvar_dims, "Quadvar Dims")
    call compare(out_d3_qvar_vars, in_d3_qvar_vars, in_d3_qvar_dims, "Quadvar Vars")
!    call compare(out_d3_qvar_mixvars, in_d3_qvar_mixvars, "Quadvar Mixvers")
!    call compare(out_d3_qvar_mixlen, in_d3_qvar_mixlen, "Quadvar Mixlen")
    call compare(DB_DOUBLE, in_d3_qvar_datatype, "Quadvar Datatype")
    call compare(DB_ZONECENT, in_d3_qvar_centering, "Quadvar Zonecent")

    call compare(out_d3_qvar_coordsys, in_d3_qvar_coordsys, "Quadvar Coordsys")
    call compare(out_d3_qvar_cycle, in_d3_qvar_cycle, "Quadvar Cycle")
    call compare(out_d3_qvar_majororder, in_d3_qvar_majororder, "Quadvar Major Order")
    call compare(out_d3_qvar_origin, in_d3_qvar_origin, "Quadvar Origin")
    call compare(out_d3_qvar_time, in_d3_qvar_time, "Quadvar time")
    call compare(out_d3_qvar_dtime, in_d3_qvar_dtime, "Quadvar dtime")
    call compare(out_d3_qvar_asciilabel, in_d3_qvar_asciilabel, "Quadvar ASCII Label")
    call compare(out_d3_Qvar_conserved, in_d3_qvar_conserved, "Quadvar Conserved")
    call compare(out_d3_Qvar_extensive, in_d3_qvar_extensive, "Quadvar Extensive")
    call compare(out_d3_qvar_guihide, in_d3_Qvar_guihide, "Quadvar GUI Hide")

    print *, "About to get multimesh"
!    print *, in_mmesh_name

    err = dbgetmmesh(file1, out_mmesh_name, in_nmmesh_mesh, in_mmesh_meshnames, in_mmesh_meshtypes, in_mmesh_optlist)
    call check_error(err)

    ! TODO: The 3 commented ones seg fault.
    err = dbgetopt(in_mmesh_optlist, DBOPT_BLOCKORIGIN, in_mmesh_blockorigin)
    call check_error(err)
!    err = dbgetopt(in_mmesh_optlist, DBOPT_CYCLE, in_mmesh_cycle)
!    call check_error(err)
!    err = dbgetopt(in_mmesh_optlist, DBOPT_TIME, in_mmesh_time)
!    call check_error(err)
!    err = dbgetopt(in_mmesh_optlist, DBOPT_DTIME, in_mmesh_dtime)
!    call check_error(err)
    err = dbgetopt(in_mmesh_optlist, DBOPT_HIDE_FROM_GUI, in_mmesh_guihide)
    call check_error(err)
    err = dbgetopt(in_mmesh_optlist, DBOPT_EXTENTS_SIZE, in_mmesh_extentssize)
    call check_error(err)
    err = dbgetopt(in_mmesh_optlist, DBOPT_TV_CONNECTIVITY, in_mmesh_tvconnectivity)
    call check_error(err)
    err = dbgetopt(in_mmesh_optlist, DBOPT_DISJOINT_MODE, in_mmesh_disjoint)
    call check_error(err)
    err = dbgetopt(in_mmesh_optlist, DBOPT_TOPO_DIM, in_mmesh_topodim)
    call check_error(err)
    err = dbgetopt(in_mmesh_optlist, DBOPT_MB_BLOCK_TYPE, in_mmesh_mbblocktype)
    call check_error(err)
    err = dbgetopt(in_mmesh_optlist, DBOPT_MB_EMPTY_COUNT, in_mmesh_mbemptycount)
    call check_error(err)

    call compare(out_mmesh_mesh, in_nmmesh_mesh, "Multimesh NMesh")
    call compare(out_mmesh_meshtypes, in_mmesh_meshtypes, "Meshtypes")
    do iter = 1, in_nmmesh_mesh, 1
        call compare(out_mmesh_meshnames(iter), in_mmesh_meshnames(iter), "Multimesh meshnames")
    end do
    call compare(out_mmesh_blockorigin, in_mmesh_blockorigin, "Multimesh Block Origin")
    call compare(out_mmesh_cycle, in_mmesh_cycle, "Multimesh Cycle")
    call compare(out_mmesh_time, in_mmesh_time, "Multimesh Time")
    call compare(out_mmesh_dtime, in_mmesh_dtime, "Multimesh Dtime")
    call compare(out_mmesh_extentssize, in_mmesh_extentssize, "Multimesh Extents Size")
    call compare(out_mmesh_guihide, in_mmesh_guihide, "Multimesh  GUI Hide")
    call compare(out_mmesh_tvconnectivity, in_mmesh_tvconnectivity, "Multimesh TV")
    call compare(out_mmesh_disjoint, in_mmesh_disjoint, "Multimesh Disjoint Mode")
    call compare(out_mmesh_topodim, in_mmesh_topodim, "Multimesh Topological Dimension")
    call compare(out_mmesh_mbblocktype, in_mmesh_mbblocktype, "Multimesh Block type")
    call compare(out_mmesh_mbemptycount, in_mmesh_mbemptycount, "Multimesh Empty Count")

    !! MULTIVAR

    err = dbgetmvar(file1, out_mvar_name, in_mvar_nvar, in_mvar_varnames, in_mvar_vartypes, in_mvar_optlist)
    call check_error(err)

    call compare(out_mvar_nvar, in_mvar_nvar, "Multivar nvars")
    call compare(out_mvar_vartypes, in_mvar_vartypes, "Vartypes")

    do iter = 1, in_mvar_nvar, 1
        call compare(out_mvar_varnames(iter), in_mvar_varnames(iter), "Multimesh meshnames")
    end do

    err = dbgetopt(in_mvar_optlist, DBOPT_BLOCKORIGIN, in_mvar_blockorigin)
    call check_error(err)
!    err = dbgetopt(in_mvar_optlist, DBOPT_CYCLE, in_mvar_cycle)
!    call check_error(err)
!    err = dbgetopt(in_mvar_optlist, DBOPT_TIME, in_mvar_time)
!    call check_error(err)
!    err = dbgetopt(in_mvar_optlist, DBOPT_DTIME, in_mvar_dtime)
!    call check_error(err)
    err = dbgetopt(in_mvar_optlist, DBOPT_EXTENTS_SIZE, in_mvar_extentssize)
    call check_error(err)
    err = dbgetopt(in_mvar_optlist, DBOPT_HIDE_FROM_GUI, in_mvar_guihide)
    call check_error(err)
    err = dbgetopt(in_mvar_optlist, DBOPT_TENSOR_RANK, in_mvar_tensorrank)
    call check_error(err)
    err = dbgetopt(in_mvar_optlist, DBOPT_CONSERVED, in_mvar_conserved)
    call check_error(err)
    err = dbgetopt(in_mvar_optlist, DBOPT_EXTENSIVE, in_mvar_extensive)
    call check_error(err)
    err = dbgetopt(in_mvar_optlist, DBOPT_MB_BLOCK_TYPE, in_mvar_mbblocktype)
    call check_error(err)
!    err = dbgetopt(in_mvar_optlist, DBOPT_MB_EMPTY_LIST, in_mvar_emptylist)
!    call check_error(err)
    err = dbgetopt(in_mvar_optlist, DBOPT_MISSING_VALUE, in_mvar_missing)
    call check_error(err)
    err = dbgetopt(in_mvar_optlist, DBOPT_MB_EMPTY_COUNT, in_mvar_emptycount)
    call check_error(err)

    call compare(out_mvar_blockorigin, in_mvar_blockorigin, "Multivar Block Origin")
    call compare(out_mvar_cycle, in_mvar_cycle, "Multivar Cycle")
    call compare(out_mvar_time, in_mvar_time, "Multivar Time")
    call compare(out_mvar_dtime, in_mvar_dtime, "Multivar Dtime")
    call compare(out_mvar_extentssize, in_mvar_extentssize, "Multivar Extents Size")
    call compare(out_mvar_guihide, in_mvar_guihide, "Multivar  GUI Hide")
    call compare(out_mvar_tensorrank, in_mvar_tensorrank, "Multivar Tensor Rank")
    call compare(out_mvar_conserved, in_mvar_conserved, "Multivar Conserved")
    call compare(out_mvar_extensive, in_mvar_extensive, "Multivar Extensive")
    call compare(out_mvar_mbblocktype, in_mvar_mbblocktype, "Multivar Block type")
!    call compare(out_mvar_emptylist, in_mvar_emptylist, "Multivar Empty List") ! TODO: Deal with unallocated arrays
    call compare(out_mvar_missing, in_mvar_missing, "Multivar Missing Value")
    call compare(out_mvar_emptycount, in_mvar_emptycount, "Multivar Empty Count")

    print *, "+-------------------------------------------------------------+"
    print *, "| Summary                                                     |"
    print *, "+-------------------------------------------------------------+"

    call complete_test

    err = lib_test_()
    print *, "Result: ", err

end program testsilo
