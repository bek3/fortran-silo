module testdata_out
    implicit none
!    include "silo_f9x.inc"

    ! Filenames
    character(len=14) :: filename1 = "testfile1.silo"
    character(len=14) :: filename2 = "testfile2.silo"
    integer :: file1

    !!!!!!!!!!!!!!!!!! QUADMESH !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    ! Quadmesh 2 info (Double Precision Coords)
    character(len=28) :: d3_qmesh_desc = "Test Mesh 2 - DP Coords - 3D"
    character(len=15) :: d3_qmesh_name = "tst_dp_qmesh_3d"
    integer :: out_d3_qmesh_ndims = 3
    integer, dimension(3) :: out_d3_qmesh_dims = (/3, 5, 7/)
    real(kind=8), dimension(3) :: out_d3_qmesh_x = (/1.123456789, 2.987654321, 3.192837465/)
    real(kind=8), dimension(5) :: out_d3_qmesh_y = (/2.192837465, 4.129834765, 6.123987456, 8.5436782190, &
            & 10.129348567/)
    real(kind=8), dimension(7) :: out_d3_qmesh_z = (/1.000000001, 2.0000000002, 9.0000000005, 18.00000000009, &
            & 53.000000001, 101.000000001, 1111.000000001/)
    character(len=6) :: out_d3_qmesh_xname = "X Axis"
    character(len=6) :: out_d3_qmesh_yname = "Y Axis"
    character(len=6) :: out_d3_qmesh_zname = "Z Axis"
    integer :: out_d3_qmesh_optlist     ! Optlist to be set later

    ! Quadmesh 2 options
    integer :: out_d3_qmesh_groupno = 7
    integer :: out_d3_qmesh_cycle = 3
    integer :: out_d3_qmesh_coordsys = 1
    integer :: out_d3_qmesh_majororder = 1
    ! TODO: Stride
    ! TODO: Coordtype
    integer :: out_d3_qmesh_facetype = 130  ! DB_QUADRECT
    integer :: out_d3_qmesh_planar = 141    ! DB_VOLUME
    real(kind=4) :: out_d3_qmesh_time = real(1928374675.32534551345624, 4)
    real(kind=8) :: out_d3_qmesh_dtime = 1928374675.32534551345624
    ! TODO: mout_extents
    ! TODO: Max Extents
    ! TODO: xlabel
    ! TODO: ylabel
    ! TODO: zlabel
    ! TODO: xunits
    ! TODO: yunits
    ! TODO: zunits
    integer :: out_d3_qmesh_nspace = 3
    ! TODO: nnodes
    integer :: out_d3_qmesh_origin = 1
    ! TODO: min index
    ! TODO: max index
    integer, dimension(3) :: out_d3_qmesh_baseindex = (/1, 1, 1/)
    ! TODO: start index
    ! TODO: size index
    ! TODO: guihide
    ! TODO: mrgtree name
    ! TODO: Ghost name labels

    ! Quadvar 2: Double Precision, 3D
    character(len=14) :: out_d3_qvar_name = "tst_dp_qvar_3d"
    character(len=15) :: out_d3_qvar_meshname = "tst_dp_qmesh_3d"
    integer :: out_d3_qvar_nvars = 1
    character(len=1), dimension(1) :: out_d3_qvar_varnames = (/"h"/)
    integer, dimension(1) :: out_d3_qvar_lvarnames = (/1/)
    real(kind=8), pointer :: out_d3_qvar_vars(:,:,:) => null()
    integer, dimension(3) :: out_d3_qvar_dims = (/3, 5, 7/)
    integer :: out_d3_qvar_ndims = 3
    character, pointer :: out_d3_qvar_mixvars => null()
    integer :: out_d3_qvar_mixlen = 0
    integer :: out_d3_qvar_optlist  ! optlist to be set later

    ! Quadvar Options
    integer :: out_d3_qvar_coordsys = 120
    integer :: out_d3_qvar_cycle = 38
    integer :: out_d3_qvar_facetype = 101
    integer :: out_d3_qvar_majororder = 1
    integer :: out_d3_Qvar_origin = 1
    real(kind=4) :: out_d3_Qvar_time = real(352.35124643634623463426, 4)
    real(kind=8) :: out_d3_qvar_dtime = real(352.35124643634623463426, 8)
    integer :: out_d3_Qvar_usespecmf = 1000
    integer :: out_d3_qvar_asciilabel = 1
    integer :: out_d3_qvar_conserved = 1
    integer :: out_d3_qvar_extensive = 1
    integer :: out_d3_qvar_guihide = 1

    ! Multimesh
    character(len=13) :: out_mmesh_name = "tst_multimesh"
    integer :: out_mmesh_mesh = 3
    character(len=8), dimension(:), allocatable :: out_mmesh_meshnames
    integer, dimension(3) :: out_mmesh_meshname_len = (/7, 6, 5/)
    integer, dimension(3) :: out_mmesh_meshtypes = (/130, 130, 130/)
    integer :: out_mmesh_optlist

    ! Multimesh Options
    integer :: out_mmesh_blockorigin = 2
    integer :: out_mmesh_cycle = 2
    real(kind=4) :: out_mmesh_time = real(352.351223463453457426, 4)
    real(kind=8) :: out_mmesh_dtime = real(352.351223463453457426, 8)
    integer :: out_mmesh_extentssize = 1
    real(kind=4), pointer :: out_mmesh_extents  ! TODO: Look into testing this
    integer, pointer :: out_mmesh_zonecounts    ! TODO: Testing
    integer, pointer :: out_mmesh_hasexternal   ! TODO: test
    integer :: out_mmesh_guihide = 1
    ! TODO: MRGTREE Name
    integer :: out_mmesh_tvconnectivity = 1
    integer :: out_mmesh_disjoint = 98
    integer :: out_mmesh_topodim = 1
    integer :: out_mmesh_mbblocktype = 2
    ! TODO: MB File Namescheme
    ! TODO: BM Block Namescheme
    integer, pointer :: out_mmesh_emptylist     ! TODO: Test
    integer :: out_mmesh_mbemptycount = 2

    ! Multivar
    character(len=12) :: out_mvar_name = "tst_multivar"
    integer :: out_mvar_nvar = 3
    character(len=8), dimension(:), allocatable :: out_mvar_varnames
    integer, dimension(3) :: out_mvar_varname_len = (/7, 6, 5/)
    integer, dimension(3) :: out_mvar_vartypes = (/130, 130, 130/)
    integer :: out_mvar_optlist

    ! Multivar Options
    integer :: out_mvar_blockorigin = 3
    integer :: out_mvar_cycle = 4
    real(kind=4) :: out_mvar_time = real(372.351223463453457426, 4)
    real(kind=8) :: out_mvar_dtime = real(372.351223463453457426, 8)
    integer :: out_mvar_guihide = 1
    integer :: out_mvar_extentssize = 2
    real(kind=4), pointer :: out_mvar_extents   ! TODO
    ! TODO: MMesh name
    integer :: out_mvar_tensorrank = 40
    ! TODO: Region PNames
    integer :: out_mvar_conserved = 1
    integer :: out_mvar_extensive = 1
    integer :: out_mvar_mbblocktype = 3
    ! TODO: MB File NS
    ! TODO: MB Block NS
    integer, pointer :: out_mvar_emptylist
    integer :: out_mvar_emptycount = 3
    real(kind=8) :: out_mvar_missing = 4.63

    interface populate_arr
!        module procedure populate_arr_3d_sp
        module procedure populate_arr_3d_dp
!        module procedure populate_arr_2d_sp
!        module procedure populate_arr_2D_dp
!        module procedure populate_arr_1d_sp
!        module procedure populate_arr_1d_dp
    end interface populate_arr

    contains

        integer function populate_arr_3d_dp(array, dims) result(err)
            implicit none

            real(kind=8), pointer, intent(out) :: array(:,:,:)
            integer, dimension(3), intent(in) :: dims
            integer :: x_iter, y_iter, z_iter

            err = 0

            allocate(array(dims(1), dims(2), dims(3)))

            call srand(12345678)

            do x_iter = 1, dims(1), 1
                do y_iter = 1, dims(2), 1
                    do z_iter = 1, dims(3), 1
                        array(x_iter, y_iter, z_iter) = x_iter + y_iter * z_iter
                    end do
                end do
            end do

        end function populate_arr_3d_dp

end module testdata_out
