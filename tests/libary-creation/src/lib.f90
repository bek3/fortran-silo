module lib_f
      implicit none

      integer, parameter :: CONSTANT_F = 5

      contains

      integer function test_f() result(iout)
          implicit none

          iout = CONSTANT_F
      end function

end module
